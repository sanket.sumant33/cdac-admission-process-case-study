/*
 * load_students.h
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#ifndef LOAD_STUDENTS_H_
#define LOAD_STUDENTS_H_

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "Student.h"


using namespace std;

vector<Student> vstudents;

vector<Student> load_students(){
	ifstream fp;
	string line;
	int c;
	fp.open("students.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[13];
		for(int i=0; i<13; i++)
			getline(str, tokens[i], ',');
		Student studentobj(stoi(tokens[0]), tokens[1], stoi(tokens[2]),stoi(tokens[3]),stoi(tokens[4]),tokens[5], stod(tokens[6]),stoi(tokens[7]),tokens[8],tokens[9],stod(tokens[10]),stoi(tokens[11]),tokens[12]);
		vstudents.push_back(studentobj);
		c++;
	}
	fp.close();

	cout << "students loaded: " << c << endl;

	return vstudents;
}

#endif /* LOAD_STUDENTS_H_ */
