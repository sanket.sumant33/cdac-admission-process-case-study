/*
 * load_centers.h
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#ifndef LOAD_CENTERS_H_
#define LOAD_CENTERS_H_

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "Center.h"

using namespace std;

vector<Center> vcenters;

vector<Center> load_centers(){
	ifstream fp;
	string line;
	int c;
	fp.open("centers.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[5];
		for(int i=0; i<5; i++)
			getline(str, tokens[i], ',');
		Center centerobj(tokens[0], tokens[1] ,tokens[2],tokens[3],tokens[4]);
		vcenters.push_back(centerobj);
		c++;
	}
	fp.close();

	cout << "centers loaded: " << c << endl;

	return vcenters;
}


#endif /* LOAD_CENTERS_H_ */
