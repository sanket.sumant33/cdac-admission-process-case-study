/*
 * load_courses.h
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#ifndef LOAD_COURSES_H_
#define LOAD_COURSES_H_

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "Course.h"

using namespace std;

vector<Course> vcourses;

vector<Course> load_courses(){
	ifstream fp;
	string line;
	int c;
	fp.open("courses.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[4];
		for(int i=0; i<4; i++)
			getline(str, tokens[i], ',');
		Course courseobj(stoi(tokens[0]), tokens[1] ,stod(tokens[2]),tokens[3]);
		vcourses.push_back(courseobj);
		c++;
	}
	fp.close();

	cout << "courses loaded: " << c << endl;

	return vcourses;
}





#endif /* LOAD_COURSES_H_ */
