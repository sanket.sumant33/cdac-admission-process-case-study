/*
 * load_eligibility.h
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#ifndef LOAD_ELIGIBILITIES_H_
#define LOAD_ELIGIBILITIES_H_

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "Eligibility.h"
#include "Course.h"

using namespace std;

vector<Eligibility> veligibilities;

vector<Eligibility> load_eligibilities(){
	ifstream fp;
	string line;
	int c;
	fp.open("eligibilities.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[3];
		for(int i=0; i<3; i++)
			getline(str, tokens[i], ',');
		Eligibility eligibilityobj(tokens[0],tokens[1],stod(tokens[2]));
		veligibilities.push_back(eligibilityobj);
		c++;
	}
	fp.close();

	cout << "eligibilities loaded: " << c << endl;

	return veligibilities;
}

Course* find_ccourse(vector<Course>& vcourses, string course_name) {
	unsigned i;
	for(i=0; i<vcourses.size(); i++) {
		if(vcourses[i].getName() == course_name)
			return &vcourses[i];
	}
	return NULL;
}

vector<Eligibility> load_eligibilities_with_courses(vector<Course>& vcourses){
	ifstream fp;
	string line;
	int count;
	fp.open("eligibilities.csv");
	if(!fp) {
		perror("failed to open file");
	}

	count=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[3];
		for(int i=0; i<3; i++)
			getline(str, tokens[i], ',');
		Eligibility eligibilityobj(tokens[0],tokens[1],stod(tokens[2]));
		Course *c=find_ccourse(vcourses,eligibilityobj.getCourseName());
		c->getEligibilityvector().push_back(eligibilityobj);

		count++;
	}
	fp.close();

	cout << "eligibilities loaded: " << count << endl;

	return veligibilities;
}




#endif /* LOAD_ELIGIBILITIES_H_ */
