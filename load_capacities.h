/*
 * load_capacities.h
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#ifndef LOAD_CAPACITIES_H_
#define LOAD_CAPACITIES_H_

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>

#include "Capacity.h"
#include "Center.h"
#include "Course.h"

using namespace std;

vector<Capacity> vcapacities;

Center* find_center(vector<Center>& vcenters, string center_id) {
	unsigned i;
	for(i=0; i<vcenters.size(); i++) {
		if(vcenters[i].getCenterId() == center_id)
			return &vcenters[i];
	}
	return NULL;
}

Course* find_course(vector<Course>& vcourses, string course_name) {
	unsigned i;
	for(i=0; i<vcourses.size(); i++) {
		if(vcourses[i].getName() == course_name)
			return &vcourses[i];
	}
	return NULL;
}

vector<Capacity> load_capacities(){
	ifstream fp;
	string line;
	int c;
	fp.open("capacities.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[4];
		for(int i=0; i<4; i++)
			getline(str, tokens[i], ',');
		Capacity capacityobj(tokens[0], tokens[1], stoi(tokens[2]),stoi(tokens[3]));
		vcapacities.push_back(capacityobj);
		c++;
	}
	fp.close();

	cout << "capacities loaded: " << c << endl;

	return vcapacities;
}

vector<Capacity> load_capacities_withcenterandcourse(vector<Center> vcenters,vector<Course> vcourses){
	ifstream fp;
	string line;
	int c;
	fp.open("capacities.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[4];
		for(int i=0; i<4; i++)
			getline(str, tokens[i], ',');
		Capacity capacityobj(tokens[0], tokens[1], stoi(tokens[2]),stoi(tokens[3]));
		vcapacities.push_back(capacityobj);

		Center *ccenter = find_center(vcenters,capacityobj.getCenterId());
		ccenter->coursecapacity[capacityobj.getCourseName()]=vcapacities.size()-1;

		Course *ccourse = find_course(vcourses,capacityobj.getCourseName());
		ccourse->centercapacity[capacityobj.getCenterId()] = vcapacities.size()-1;

		c++;
	}
	fp.close();

	cout << "capacities loaded: " << c << endl;

	return vcapacities;
}

#endif /* LOAD_CAPACITIES_H_ */
