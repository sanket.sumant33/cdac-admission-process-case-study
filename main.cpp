/*
 * main.cpp
 *
 *  Created on: 15-Apr-2020
 *      Author: sunbeam
 */

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "Student.h"
#include "Preference.h"
#include "Eligibility.h"
#include "Center.h"
#include "Course.h"
#include "Capacity.h"

#include "load_students.h"
#include "load_preferences.h"
#include "load_eligibilities.h"
#include "load_centers.h"
#include "load_courses.h"
#include "load_capacities.h"

using namespace std;

int main()
{
	unsigned i;



	//File Loading

	//========================================================//

	/*vector<Student> vstudents=load_students();

	 for(i=0; i<vstudents.size(); i++)
		vstudents[i].display();*/

	/*vector<Preference> vpreferences=load_preferences();

	for(i=0; i<vpreferences.size(); i++)
		vpreferences[i].display();*/

	/*vector<Eligibility> veligibilities=load_eligibilities();

	 for(i=0; i<veligibilities.size(); i++)
		veligibilities[i].display();*/

	/*vector<Center> vcenters=load_centers();

	for(i=0; i<vcenters.size(); i++)
		vcenters[i].display();*/

	/*vector<Course> vcourses=load_courses();

	for(i=0; i<vcourses.size(); i++)
		vcourses[i].display();*/

	/*vector<Capacity> vcapacities=load_capacities();

		for(i=0; i<vcapacities.size(); i++)
			vcapacities[i].display();*/

	//========================================================//



	//File Joining

	//========================================================//

	/*vector<Course> vcourses=load_courses();

	vector<Eligibility> veligibilities=load_eligibilities_with_courses(vcourses);

	for(i=0; i<vcourses.size(); i++){
		vcourses[i].display();
		vcourses[i].display_eligibilities();
	}*/

	/*vector<Student> vstudents=load_students();

	vector<Preference> vpreferences=load_preferences_with_students(vstudents);

	for(i=0; i<vstudents.size(); i++){
		vstudents[i].display();
		vstudents[i].display_preferences();
	}*/

	vector<Center> vcenters=load_centers();
	vector<Course> vcourses=load_courses();

	vector<Capacity> vcapacities=load_capacities_withcenterandcourse(vcenters,vcourses);

	for(i=0; i<vcourses.size(); i++){
		vcourses[i].display();
		vcourses[i].display_capacity(vcapacities);
	}

	for(i=0; i<vcenters.size(); i++){
		vcenters[i].display();
		vcenters[i].display_capacity(vcapacities);
	}

	//========================================================//
	return 0;
}

