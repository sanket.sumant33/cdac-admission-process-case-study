/*
 * load_preferences.h
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#ifndef LOAD_PREFERENCES_H_
#define LOAD_PREFERENCES_H_

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include "Preference.h"
#include "Student.h"

using namespace std;

vector<Preference> vpreferences;

vector<Preference> load_preferences(){
	ifstream fp;
	string line;
	int c;
	fp.open("preferences.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[4];
		for(int i=0; i<4; i++)
			getline(str, tokens[i], ',');
		Preference preferenceobj(stoi(tokens[0]), stoi(tokens[1]),tokens[2],tokens[3]);
		vpreferences.push_back(preferenceobj);
		c++;
	}
	fp.close();

	cout << "Preferences loaded: " << c << endl;

	return vpreferences;
}

Student* find_student(vector<Student>& vstudnets, int form_no) {
	unsigned i;
	for(i=0; i<vstudnets.size(); i++) {
		if(vstudnets[i].getform_no() == form_no)
			return &vstudnets[i];
	}
	return NULL;
}

vector<Preference> load_preferences_with_students(vector<Student>& vstudents){
	ifstream fp;
	string line;
	int c;
	fp.open("preferences.csv");
	if(!fp) {
		perror("failed to open file");
	}

	c=0;
	while(getline(fp, line)) {
		stringstream str(line);
		string tokens[4];
		for(int i=0; i<4; i++)
			getline(str, tokens[i], ',');
		Preference preferenceobj(stoi(tokens[0]), stoi(tokens[1]),tokens[2],tokens[3]);
		Student *s = find_student(vstudents, preferenceobj.getStudentFormno());
		s->getPreferencevector().push_back(preferenceobj);

		c++;
	}
	fp.close();

	cout << "Preferences loaded: " << c << endl;

	return vpreferences;
}


#endif /* LOAD_PREFERENCES_H_ */
